#!/bin/bash

url_api=`curl -v --silent http://a677f8823ece44f13bb8a8fdd2a42e1e-6ac1d5ac829c74f1.elb.us-east-1.amazonaws.com/cabralpc-api 2>&1 | grep "1.0," | cut -d: -f 2 | cut -d" " -f 2`
version="1.0,"

if [ $url_api = $version ]; then
    echo "Api OK!!"

    echo "Buildando imagem com nova tag para um rollback futuro"
         
    docker build -t $NAME_IMAGEM .
    docker tag $NAME_IMAGEM cabralpc/$NAME_IMAGEM:roll_back
    docker push cabralpc/$NAME_IMAGEM:roll_back
else
   echo "Api com versao irregular, fazendo rollback......"
      
   envsubst < deployment_api_rollback.yaml | kubectl apply -f -
   
   sleep 30
   echo "Rollback realizado com sucesso"
   
   curl http://a677f8823ece44f13bb8a8fdd2a42e1e-6ac1d5ac829c74f1.elb.us-east-1.amazonaws.com/cabralpc-api
fi

