from flask import Flask,  jsonify
import boto3
import mysql.connector
from mysql.connector import Error
import redis
from flask import jsonify
import os 

app = Flask(__name__)

   
@app.route('/redis')
def redis_nosql():
    r = redis.StrictRedis(
        host='54.224.149.216',
        port=6379,
        password=''
        #ssl=True
    )
    #test the connection
    r.set('hello', 'Conexao ok')
    value = r.get('hello')
    print(value)
    return 'Conexao OK' 


@app.route('/fila')
def fila():
   
    sqs = boto3.client('sqs' , region_name=os.getenv('AWS_DEFAULT_REGION'), aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'), aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY'))
    response = sqs.get_queue_url(QueueName='filadesafio4')
    response = sqs.list_queues()
    return(f"Filas ok, {response['QueueUrls']} " )
    
  

  
@app.route('/mysql')
def status_mysql():
    conn = None
    try:
        conn = mysql.connector.connect(host=os.getenv('HOST_MYSQL_ENV'),
                                       database=os.getenv('MYSQL_BASE_ENV'),
                                       user=os.getenv('MYSQL_USER_ENV'),
                                       password=os.getenv('MYSQL_PASSWORD_ENV'))
                                       
                                   
        if conn.is_connected():
            return'Conexao OK'

    except Error as e:
        return "erro 400"

    finally:
        if conn is not None and conn.is_connected():
            conn.close() 
#rotas
@app.route('/cabralpc-api')
def list_all():
        desafio4 = {
           'api' : 1.0,
           'dep' : {
           'sql' : status_mysql(),
           'nosql' : redis_nosql(),
           'fila' : fila(),

        },
          }
        msg ={
        'API': desafio4,
        'Status': 200    
    }
        resp = jsonify(msg)
        return resp

        

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')   
